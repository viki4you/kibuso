import datetime
from flask import Flask, request, send_from_directory, render_template, redirect, url_for, flash, Response, send_file
from flask_babelex import Babel
from flask_sqlalchemy import SQLAlchemy
from flask_user import current_user, login_required, roles_required, UserManager, UserMixin

from flask_user.forms import OrderForm, AGBsForm, ContinueForm
import secrets
from os import listdir
from os.path import isfile, join
import ftplib

from ftplib import FTP
from threading import Thread, Event
from queue import Queue, Empty
from io import BytesIO
from credentials import *

price = '6.95'
send_back = True
send_back_price = '0.00'
amount=1

# Class-based application configuration
class ConfigClass(object):
    """ Flask application config """
    BABEL_DEFAULT_LOCALE = 'de'

    # Flask settings
    SECRET_KEY = secrets.token_hex(16)

    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'sqlite:///basic_app.sqlite'  # File-based SQL database
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # Avoids SQLAlchemy warning

    # Flask-Mail SMTP server settings
    MAIL_SERVER = 'smtps.udag.de'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USE_TLS = False
    MAIL_USERNAME = 'info@vhs-kassette-digitalisieren.de'
    MAIL_PASSWORD = 'Viki$lot7'
    MAIL_DEFAULT_SENDER = '"Web-Master" <info@vhs-kassette-digitalisieren.de>'

    # Flask-User settings
    USER_APP_NAME = "vhs-kassette-digitalisieren.de"  # Shown in and email templates and page footers
    USER_ENABLE_EMAIL = True  # Enable email authentication
    USER_ENABLE_USERNAME = False  # Disable username authentication
    USER_EMAIL_SENDER_NAME = USER_APP_NAME
    USER_EMAIL_SENDER_EMAIL = "info@vhs-kassette-digitalisieren.de"

# Create Flask app load app.config
app = Flask(__name__)

app.config.from_object(__name__ + '.ConfigClass')
# Initialize Flask-BabelEx
babel = Babel(app, default_locale='de')

# Use the browser's language preferences to select an available translation
@babel.localeselector
def get_locale():
    translations = [str(translation) for translation in babel.list_translations()]
    return request.accept_languages.best_match(translations)

# Initialize Flask-SQLAlchemy
db = SQLAlchemy(app)

# Define the User data-model.
# NB: Make sure to add flask_user_customized UserMixin !!!
class User(db.Model, UserMixin):
    __tablename__ = 'users'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')

    # User authentication information. The collation='NOCASE' is required
    # to search case insensitively when USER_IFIND_MODE is 'nocase_collation'.
    email = db.Column(db.String(255, collation='NOCASE'), nullable=False, unique=True)
    email_confirmed_at = db.Column(db.DateTime())
    password = db.Column(db.String(255), nullable=False, server_default='')

    # User information
    first_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    last_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    street = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    housenumber = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    adresshint = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    postcode = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    city = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    country = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')

    # Define the relationship to Role via UserRoles
    roles = db.relationship('Role', secondary='user_roles')

    def __repr__(self):
        return '<User email %r>' % self.email

class SendBackPrice(db.Model):
    __tablename__ = 'send_back_price'
    id = db.Column(db.Integer, primary_key=True)
    send_back_price = db.Column(db.String(255, collation='NOCASE'), nullable=False, unique=False)
    amount = db.Column(db.String(255, collation='NOCASE'), nullable=False, unique=False)

    def __repr__(self):
        return '<SendBackPrice %r>' % self.send_back_price

class Capacity(db.Model):
    __tablename__ = 'capacity'
    id = db.Column(db.Integer, primary_key=True)
    capacity_left = db.Column(db.String(255, collation='NOCASE'), nullable=False, unique=False)

    def __repr__(self):
        return '<Capacity %r>' % self.capacity_left

    # Define the Order data-model.
    # NB: Make sure to add flask_user_customized UserMixin !!!
class Order(db.Model):
    __tablename__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)

    # Order information. The collation='NOCASE' is required
    # to search case insensitively when USER_IFIND_MODE is 'nocase_collation'.
    email = db.Column(db.String(255, collation='NOCASE'), nullable=False, unique=False)
    order_date = db.Column(db.DateTime())
    order_description = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    order_amount = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    order_invoice_link = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    order_download_link = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    order_instructions_link = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')

    # User information
    first_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    last_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    street = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    housenumber = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    adresshint = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    postcode = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    city = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    country = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    send_back = db.Column('send_back', db.Boolean(), nullable=False, server_default='1')
    payed = db.Column('payed', db.Boolean(), nullable=False, server_default='-1')
    right_of_withdrawal = db.Column('right_of_withdrawal', db.Boolean(), nullable=False, server_default='-1')
    waiver_of_right_of_withdrawal = db.Column('waiver_of_right_of_withdrawal', db.Boolean(), nullable=False, server_default='-1')
    agbs = db.Column('agbs', db.Boolean(), nullable=False, server_default='-1')

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('orders', lazy=True))

    def __repr__(self):
        return '<Order email %r>' % self.email

# Define the Role data-model
class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)

# Define the UserRoles association table
class UserRoles(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))

# Setup Flask-User and specify the User data-model
user_manager = UserManager(app, db, User)

# Create all database tables
db.create_all()

# Create initiall value for capacity
if Capacity.query.filter(Capacity.capacity_left).first() == None:
    capacity = Capacity(
        capacity_left=-1
    )
    db.session.add(capacity)
    db.session.commit()

# Create 'member@example.com' user with no roles
if not User.query.filter(User.email == 'member@example.com').first():
    user = User(
        email='member@example.com',
        email_confirmed_at=datetime.datetime.utcnow(),
        password=user_manager.hash_password('Password1'),
    )
    db.session.add(user)
    db.session.commit()

# Create 'admin@example.com' user with 'Admin' and 'Agent' roles
if not User.query.filter(User.email == 'admin@example.com').first():
    user = User(
        email='admin@example.com',
        email_confirmed_at=datetime.datetime.utcnow(),
        password=user_manager.hash_password('Password1'),
    )
    user.roles.append(Role(name='Admin'))
    user.roles.append(Role(name='Agent'))
    db.session.add(user)
    db.session.commit()

# The Home page is accessible to anyone
@app.route('/', methods=['GET', 'POST'])
def home_page():
    form = ContinueForm()
    form.submit.label.text = 'Bestellen'
    if request.method == 'POST' and form.validate():
        return redirect(url_for('order_page'))
    return render_template('home_page.html', form=form, price=price)

# The Members page is only accessible to authenticated users
@app.route('/order', methods=['GET', 'POST'])
@login_required  # Use of @login_required decorator
def order_page():
    capacity = Capacity.query.with_entities(Capacity.capacity_left).first()
    if int(capacity.capacity_left) > 0:
        form = OrderForm()
        if request.method == 'POST' and form.validate():
            if len(request.form.getlist('send_back')) > 0:
                if request.form.getlist('send_back')[0] == 'y':
                    temp = 4.95
            else:
                temp = 0.0
            print('temp send_back_price:' + str(temp))
            amount = request.form.get('amount')
            print('amount:' + str(amount))
            SendBackPrice.query.delete()
            send_back_price = SendBackPrice(send_back_price=temp, amount=amount)
            db.session.add(send_back_price)
            db.session.commit()

            return redirect(url_for('user.confirm_user_profile'))
    else:
        flash("Wir sind ausverkauft.", 'error')
        return render_template('sold_out.html')
    return render_template('order_page.html', form=form, price=price)


@app.route('/confirm', methods=['GET', 'POST'])
@login_required
def confirm_page():
    form = AGBsForm()
    if request.method == 'POST' and form.validate():
        return redirect(url_for('order_completed_page'))
    send_back_price = SendBackPrice.query.with_entities(SendBackPrice.send_back_price).first().send_back_price
    amount = SendBackPrice.query.with_entities(SendBackPrice.amount).first().amount
    print(amount)
    return render_template('confirm.html', form=form, price=str("{:.2f}".format((float(price) * int(amount)) + float(send_back_price))).replace('.', ','))

@app.route('/downloads')
@login_required
def downloads():
    from os import listdir
    from os.path import isfile, join
    mypath = 'files/' + make_directory_from_user_email(current_user.email)
    if os.path.isdir(mypath):
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    else:
        onlyfiles = []
    return render_template('downloads.html', onlyfiles=onlyfiles)

@app.route('/download_content/<string:filename>')
@login_required
def download_content(filename):
    uploads = os.path.join(app.root_path, 'files/' + make_directory_from_user_email(current_user.email) + '/')
    return send_from_directory(directory=uploads, path='./' + filename, filename=filename)

@app.route('/order_completed')
@login_required
def order_completed_page():
    import os
    path = os.path.abspath(".")
    output_filename = path + 'rechnung.pdf'
    amount = SendBackPrice.query.with_entities(SendBackPrice.amount).first().amount
    end_price = (float(price) * int(amount))
    vat = end_price * 0
    send_back_price = SendBackPrice.query.with_entities(SendBackPrice.send_back_price).first().send_back_price
    vat_send_back = float(send_back_price) * 0
    text = ' \n \n \n \nIhre Rechnung von vhs-kassette-digitalisieren.de\n\n\n\n\n\n\n' \
            + 'Rechnung von:\n\n' \
            + 'kibuso\n' \
            + '\n' \
            + '\n\n\n\n\n' \
            + 'Rechnung für:\n\n' \
            + current_user.first_name + ' ' + current_user.last_name + '\n' \
            + current_user.street + ' ' + current_user.housenumber + '\n' \
            + current_user.postcode + ' ' + current_user.city + '\n' \
            + current_user.country + '\n\n\n\n\n\n\n' \
            + 'Datum: ' + datetime.datetime.utcnow().date().strftime("%d.%m.%Y") + '\n\n\n\n\n' \
            + '-------------------------------------------------------------------------------------\n\n' \
            + 'Digitalisierung der Aufnahme auf einer VHS-Kassette\t\t\t\t1x\n\n\n' \
            + 'Preis pro Einheit:\t\t\t\t\t\t\t ' + str("{:.2f}".format(float(price))).replace('.', ',') + ' Euro\n\n' \
            + 'Anzahl:\t\t\t\t\t\t\t\t\t ' + str("{:.2f}".format(float(amount))).replace('.', ',') + ' \n\n' \
            + 'Mehrwertsteuer:\t\t\t\t\t\t\t\t ' + str("{:.2f}".format(vat)).replace('.', ',') + ' Euro\n\n' \
            + 'Versandkosten:\t\t\t\t\t\t\t\t ' + str("{:.2f}".format(float(send_back_price))).replace('.', ',') + ' Euro\n\n' \
            + 'Mehrwertsteuer Versandkosten:\t\t\t\t\t\t ' + str("{:.2f}".format(vat_send_back)).replace('.', ',') + ' Euro\n\n' \
            + '-------------------------------------------------------------------------------------\n\n' \
            + 'Gesamt:\t\t\t\t\t\t\t\t\t' + str("{:.2f}".format((end_price + vat + float(send_back_price) + vat_send_back))).replace('.', ',') + ' Euro\n\n\n\n\n' \
            + 'Der Betrag ist vollständig auf das folgende Konto zu überweisen:\n\n' \
            + 'IBAN: \n\n' \
            + 'Geldinstitut: \n\n\n\n' \
            + 'Der Betrag ist sofort fällig.\n\n\n\n\n' \
            + 'Mit freundlichen Grüßen \n\n\n' \
            + 'Ihr vhs-kasssette-digitalisieren.de Team \n'
    text_to_pdf(text, output_filename)
    output_filename_instructions = path + 'bestellschein.pdf'

    price_to_save = float(end_price) + float(send_back_price)
    if float(send_back_price) > 0.0:
        temp=True
    else:
        temp=False
    order = Order(email=current_user.email,
                  order_date=datetime.datetime.utcnow(),
                  order_description='Digitize video cassette',
                  order_amount=str(price_to_save),
                  order_invoice_link=output_filename,
                  order_instructions_link=output_filename_instructions,
                  first_name=current_user.first_name,
                  last_name=current_user.last_name,
                  street=current_user.street,
                  housenumber=current_user.housenumber,
                  adresshint=current_user.adresshint,
                  postcode=current_user.postcode,
                  city=current_user.city,
                  country=current_user.country,
                  user=current_user,
                  send_back=temp,
                  payed=False,
                  agbs=True,
                  waiver_of_right_of_withdrawal=True,
                  right_of_withdrawal=True)
    db.session.add(order)

    id = Order.query.with_entities(Order.id).order_by(Order.id.desc()).first().id
    text = ' \n \n \n \nIhr Bestellschein von vhs-kassette-digitalisieren.de\n\n\n\n\n\n\n' \
           + 'Bestellschein von:\n\n' \
           + 'kibuso\n' \
           + '\n' \
           + '\n\n\n\n\n' \
           + 'Ihre Daten:\n\n' \
           + current_user.first_name + ' ' + current_user.last_name + '\n' \
           + current_user.street + ' ' + current_user.housenumber + '\n' \
           + current_user.postcode + ' ' + current_user.city + '\n' \
           + current_user.country + '\n' \
           + current_user.email + '\n\n\n\n\n\n\n' \
           + 'Datum: ' + datetime.datetime.utcnow().date().strftime("%d.%m.%Y") + '\n\n\n\n\n' \
           + '-------------------------------------------------------------------------------------\n\n' \
           + 'Bestellnummer:' + str(order.id) + '\n\n\n\n' \
           + 'Mit freundlichen Grüßen \n\n\n' \
           + 'Ihr vhs-kasssette-digitalisieren.de Team \n'
    text_to_pdf(text, output_filename_instructions)

    capacity = Capacity.query.with_entities(Capacity.capacity_left).first()
    temp = int(capacity.capacity_left)
    temp -= 1
    if capacity is not None:
        Capacity.query.delete()
        capacity = Capacity(capacity_left=temp)
        db.session.add(capacity)
    db.session.commit()

    email = current_user.email
    user_manager.email_manager.send_invoice_email(current_user, email, price=str("{:.2f}".format(price_to_save)).replace('.', ','), name=current_user.last_name,
                                                  order_invoice_link=output_filename)
    user_manager.email_manager.send_instructions_email(current_user, email,
                                                  price=str("{:.2f}".format(price_to_save)).replace('.', ','),
                                                  name=current_user.last_name,
                                                  order_instructions_link=output_filename_instructions)
    flash("Wir haben eine Bestätigungsemail mit Rechnung an die Email-Adresse %s geschickt." % email, 'success')

    return render_template('order_completed.html')

import textwrap
from fpdf import FPDF

def text_to_pdf(text, filename):
    text = text.encode('latin-1', 'replace').decode('latin-1')
    a4_width_mm = 210
    pt_to_mm = 0.35
    fontsize_pt = 10
    fontsize_mm = fontsize_pt * pt_to_mm
    margin_bottom_mm = 10
    character_width_mm = 7 * pt_to_mm
    width_text = a4_width_mm / character_width_mm

    pdf = FPDF(orientation='P', unit='mm', format='A4')
    pdf.set_auto_page_break(True, margin=margin_bottom_mm)
    pdf.add_page()
    pdf.set_font(family='Courier', size=fontsize_pt)
    splitted = text.split('\n')

    for line in splitted:
        lines = textwrap.wrap(line, width_text)

        if len(lines) == 0:
            pdf.ln()

        for wrap in lines:
            pdf.cell(0, fontsize_mm, wrap, ln=1)

    pdf.output(filename, 'F')

# The Admin page requires an 'Admin' role.
@app.route('/agbs')
def agbs_page():
    return render_template('agbs.html')

@app.route('/widerrufsrecht')
def widerrufsrecht_page():
    return render_template('widerrufsrecht.html')

@app.route('/verzichts_aufs_widerrufsrecht')
def verzicht_aufs_widerrufsrecht_page():
    return render_template('verzicht_aufs_widerrufsrecht.html')

@app.route('/impressum')
def impressum_page():
    return render_template('impressum.html')

@app.route('/<path:filename>', methods=['GET', 'POST'])
@login_required
def download_file(filename):
    user_directory = make_directory_from_user_email(current_user.email)
    return send_from_directory(directory=user_directory, path=user_directory, filename=filename, as_attachment=True)

def make_directory_from_user_email(email):
    user_directory = str(email).replace('.', '__dot__')
    user_directory = user_directory.replace('@', '__at__')
    return user_directory

class FTPDownloader(object):
    def __init__(self, host, user, password, timeout=0.01, email=None):
        self.ftp = FTP(host)
        self.ftp.login(user, password)
        self.timeout = timeout
        self.email = email

    def getBytes(self, filename):
        print("getBytes")
        self.ftp.cwd('files')
        self.ftp.cwd(make_directory_from_user_email(self.email))
        self.ftp.retrbinary("RETR {}".format(filename) , self.bytes.put)
        self.bytes.join()   # wait for all blocks in the queue to be marked as processed
        self.finished.set() # mark streaming as finished

    def sendBytes(self):
        while not self.finished.is_set():
            try:
                yield self.bytes.get(timeout=self.timeout)
                self.bytes.task_done()
            except Empty:
                self.finished.wait(self.timeout)
        self.worker.join()

    def download(self, filename):
        self.bytes = Queue()
        self.finished = Event()
        self.worker = Thread(target=self.getBytes, args=(filename,))
        self.worker.start()
        return self.sendBytes()
